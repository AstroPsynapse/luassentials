# LUAssentials #

### What is it? ###

LUAssentials is a light-weight plugin written for the Lukkit plugin for Bukkit/Spigot Minecraft servers. The overall goal is to simplify what Essentials/EssentialsX is into a smaller, more modular plugin.


### Commands / Permission Nodes ###

| Permission Node                      | Command           | Description                                                     |
| ------------------------------------ | ----------------- | --------------------------------------------------------------- |
| `luassentials.admin`                 | `/luassentials`   | Controls various aspects of LUAssentials.                       |
| `luassentials.teleport.spawn`        | `/spawn`          | Teleports the player to the world's spawn point.                |
| `luassentials.teleport.tp`           | `/tp`             | Teleports to the specified player.                              |
| `luassentials.teleport.tp.other`     | `/tp`             | Can also be used to teleport one player to another player.      |
| `luassentials.teleport.back`         | `/back`           | Returns the player back to their previous position.             |
| `luassentials.home.sethome`          | `/sethome`        | Sets your current location as a home.                           |
| `luassentials.home.delhome`          | `/delhome`        | Deletes the specified home from your list of saved homes.       |
| `luassentials.home.home`             | `/home`           | Teleports to the specified home location.                       |
| `luassentials.home.home`             | `/homes`          | Lists all of the player's saved homes.                          |
| `luassentials.moderation.item.other` | `/give`           | Gives the specified player the specified item.                  |
| `luassentials.moderation.item`       | `/item`           | Grants the player the specified item.                           |
| `luassentials.moderation.mute`       | `/mute`           | Mutes or unmutes the specified player.                          |
| `luassentials.moderation.setspawn`   | `/setspawn`       | Sets the spawn point to the player's current position.          |
| `luassentials.user.motd`             | `/motd`           | Shows the server's message of the day.                          |
| `laussentials.other.clearinventory`  | `/ci`             | Completely clears a player's inventory.                         |
| `laussentials.other.lightning`       | `/lightning`      | Casts a lightning strike where the player is currently looking. |
