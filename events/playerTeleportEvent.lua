function t(event)
  if (event:getCause():toString() == "UNKNOWN") then return end -- Invalid teleport, disregard
  local pUUID = event:getPlayer():getUniqueId():toString()
  this.config.set("players."..pUUID..".lastLocation.world", event:getFrom():getWorld():getName())
  this.config.set("players."..pUUID..".lastLocation.x", event:getFrom():getX())
  this.config.set("players."..pUUID..".lastLocation.y", event:getFrom():getY()+0.5)
  this.config.set("players."..pUUID..".lastLocation.z", event:getFrom():getZ())
  this.config.set("players."..pUUID..".lastLocation.pitch", event:getFrom():getPitch())
  this.config.set("players."..pUUID..".lastLocation.yaw", event:getFrom():getYaw())
  this.config.save()
end
return t
