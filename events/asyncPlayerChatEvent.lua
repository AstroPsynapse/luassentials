function t(event)
  local sender = event:getPlayer()
  local pUUID = event:getPlayer():getUniqueId():toString()
  if (this.config.get("players."..pUUID..".muted") == true) then
    sender:sendMessage("§cYou are unable to chat because you are muted.")
    event:setCancelled(true)
    log:warning(this.prefix.." "..sender:getName() .. " tried to send a message while muted.")
  end
end
return t
