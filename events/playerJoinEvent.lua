function t(event)
  local player = event:getPlayer()
  local pUUID = player:getPlayer():getUniqueId():toString()
  local pName = player:getPlayer():getName()
  if (not this.config.get("players."..pUUID..".name")) then
    log:info(this.prefix.." Setting up data for new player "..pName)
    this.config.setDefault("players."..pUUID..".name", pName)
    this.config.setDefault("players."..pUUID..".muted", false)
    this.config.save()
  end
  -- Show MOTD
  if (this.config.get("config.showMotdOnJoin") == true) then
    player:performCommand("motd")
  end
  -- Show mute warning
  if (this.config.get("players."..pUUID..".muted") == true) then
    player:sendMessage("§cYou are unable to chat because you are muted.")
  end
  if (this.config.get("config.showRealmJoinLeaveMessages") == true) then
    event:setJoinMessage("§7§o"..pName.." enters the realm")
  end
end
return t
