function t(event)
  local player = event:getPlayer()
  if (not checkPerm(player, "luassentials.protection.build", true) and this.config.get("config.buildingAllowed") == false) then
    player:sendRawMessage(stringOf(format.RED) .. "You are not allowed to place blocks.")
    event:setCancelled(true)
  end
end
return t
