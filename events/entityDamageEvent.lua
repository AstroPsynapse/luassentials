function t(event)
  if (event:getEntity().getPlayer == nil) then return end
  local pUUID = event:getEntity():getPlayer():getUniqueId():toString()
  if (this.config.get("players."..pUUID..".godmode") == true) then
    event:setCancelled(true)
  end
end
return t
