function t(event)
  local pUUID = event:getEntity():getUniqueId():toString()
  this.config.set("players."..pUUID..".lastLocation.world", event:getEntity():getLocation():getWorld():getName())
  this.config.set("players."..pUUID..".lastLocation.x", event:getEntity():getLocation():getX())
  this.config.set("players."..pUUID..".lastLocation.y",event:getEntity():getLocation():getY())
  this.config.set("players."..pUUID..".lastLocation.z", event:getEntity():getLocation():getZ())
  this.config.set("players."..pUUID..".lastLocation.pitch", event:getEntity():getLocation():getPitch())
  this.config.set("players."..pUUID..".lastLocation.yaw", event:getEntity():getLocation():getYaw())
  this.config.save()
end
return t
