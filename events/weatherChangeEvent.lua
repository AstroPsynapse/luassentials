function t(event)
  if (this.config.get("config.announceWeatherChanges") == true) then
    if (event:toWeatherState() == true) then
      s:broadcastMessage("It begins to rain...")
    else
      s:broadcastMessage("The sky begins to clear...")
    end
  end
end
return t
