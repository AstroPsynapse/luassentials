function t(obj)
  local res = false
  if (obj.getPlayer == nil) then
    obj:sendMessage("You can not use this command from the console.")
    res = true
  end
  return res
end
return t
