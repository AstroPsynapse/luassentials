function t()
  local s = server
  local log = s:getLogger()
  local configChange = false
    if (this.config.get("config.maxHomes") == nil) then
      this.config.setDefault("config.maxHomes", 3)
      configChange = true
    end
    if (this.config.get("config.showMotdOnJoin") == nil) then
      this.config.setDefault("config.showMotdOnJoin", true)
      configChange = true
    end
    if (this.config.get("config.showRealmJoinLeaveMessages") == nil) then
      this.config.setDefault("config.showRealmJoinLeaveMessages", true)
      configChange = true
    end
    if (this.config.get("config.buildingAllowed") == nil) then
      this.config.setDefault("config.buildingAllowed", true)
      configChange = true
    end
    if (this.config.get("config.announceWeatherChanges") == nil) then
      this.config.setDefault("config.announceWeatherChanges", true)
      configChange = true
    end
    local players = tableFromList(s:getOnlinePlayers())
    if (s:getOnlinePlayers():size() > 0) then
      for i,v in pairs (players) do
        local pUUID = v:getUniqueId():toString()
        local pName = v:getName()
        if (this.config.get("players."..pUUID..".name") == nil) then
          log:info(this.prefix.." Setting up data for new player "..pName)
          this.config.setDefault("players."..pUUID..".name", pName)
          configChange = true
        end
        if (this.config.get("players."..pUUID..".muted") == nil) then
          this.config.setDefault("players."..pUUID..".muted", false)
          configChange = true
        end
        if (this.config.get("players."..pUUID..".godmode") == nil) then
          this.config.setDefault("players."..pUUID..".godmode", false)
          configChange = true
        end
      end
    end
    if (configChange) then
      log:info(this.prefix.." Config changes detected! Saving changes.")
      this.config.save()
  end
  log:info(this.prefix.." LUAssentials "..this.version.." configuration files have finished loading.")
end

return t
