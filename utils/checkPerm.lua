function t(player, permission, silent)
  if not player:hasPermission(permission) then
    if not silent then
      player:sendMessage("You do not have permission to use this command.")
      log:warning(this.prefix.." "..player:getName().." tried to use a command without proper permission. (node: "..permission..")")
    end
    return false
  else
    return true
  end
end
return t
