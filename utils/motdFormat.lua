function t(str)
  -- Variables
  formatted = str
  formatted = formatted:gsub(":player_count:", s:getOnlinePlayers():size().."/"..s:getMaxPlayers())
  formatted = formatted:gsub(":online_players:", s:getOnlinePlayers():size())
  formatted = formatted:gsub(":max_players:", s:getMaxPlayers())
  formatted = formatted:gsub(":server_version:", s:getVersion())
  formatted = formatted:gsub(":bukkit_version:", s:getBukkitVersion())
  formatted = formatted:gsub(":server_name:", s:getMotd())
  formatted = formatted:gsub(":luassentials_version:", "LUAssentials "..this.version)
  -- Colors
  formatted = formatted:gsub(":black:", "§0")
  formatted = formatted:gsub(":dark_blue:", "§1")
  formatted = formatted:gsub(":dark_green:", "§2")
  formatted = formatted:gsub(":dark_aqua:", "§3")
  formatted = formatted:gsub(":dark_red:", "§4")
  formatted = formatted:gsub(":dark_purple:", "§5")
  formatted = formatted:gsub(":gold:", "§6")
  formatted = formatted:gsub(":gray:", "§7")
  formatted = formatted:gsub(":dark_gray:", "§8")
  formatted = formatted:gsub(":blue:", "§9")
  formatted = formatted:gsub(":green:", "§a")
  formatted = formatted:gsub(":aqua:", "§b")
  formatted = formatted:gsub(":red:", "§c")
  formatted = formatted:gsub(":light_purple:", "§d")
  formatted = formatted:gsub(":yellow:", "§e")
  formatted = formatted:gsub(":white:", "§f")
  -- Formats
  formatted = formatted:gsub(":obfuscated:", "§k")
  formatted = formatted:gsub(":bold:", "§l")
  formatted = formatted:gsub(":underline:", "§m")
  formatted = formatted:gsub(":strikethrough:", "§n")
  formatted = formatted:gsub(":italic:", "§o")
  formatted = formatted:gsub(":reset:", "§r")
  -- Complete
  return formatted
end
return t
