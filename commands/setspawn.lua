function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.moderation.setspawn")) then return end
  local x = sender:getLocation():getX()
  local y = sender:getLocation():getY()
  local z = sender:getLocation():getZ()
  local setspawn = sender:getWorld():setSpawnLocation(x, y, z)
  if (setspawn == false) then return sender:sendMessage("Unable to set the world spawn point to this location.")
  else return sender:sendMessage("The world's spawn point has been updated to this location!") end
end
return t
