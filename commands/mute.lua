function t(sender, args)
  if (not checkPerm(sender, "luassentials.moderation.mute")) then return end

  if (args[1]) then
    player = server:getPlayer(args[1])
    if (player == nil) then
      sender:sendMessage("We could not find the specified player.")
      return
    -- elseif (player == sender) then
      -- sender:sendMessage("You cannot mute yourself.")
      -- return
    end

    local pUUID = player:getUniqueId():toString()
    if (this.config.get("players."..pUUID..".muted") == true) then
      this.config.set("players."..pUUID..".muted", false)
      this.config.save()
      player:sendMessage("§6You have been unmuted by a staff member.")
      sender:sendMessage("You have unmuted "..player:getName()..".")
    else
      this.config.set("players."..pUUID..".muted", true)
      this.config.save()
      player:sendMessage("§4You have been muted by a staff member.")
      sender:sendMessage("You have muted "..player:getName()..".")
    end
  end
end
return t
