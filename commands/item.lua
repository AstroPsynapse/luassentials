function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.moderation.item")) then return end
  if (#args > 0) then
    if (material:getMaterial(string.upper(args[1])) == nil) then
      sender:sendMessage("Invalid item name.")
      return
    end
    local mat = material:valueOf(string.upper(args[1]))
    local amt = 1
    if (#args > 1) then
      amt = tonumber(args[2])
      if (amt == nil) then
        sender:sendMessage("Invalid amount.")
        return
      end
    end
    itemHelpers.giveItem(sender, mat, amt)
    sender:sendMessage("Added "..amt.." "..args[1].." to your inventory")
  else
    sender:sendMessage("Invalid syntax! Usage: /item <name> [optional:amount]")
  end
end
return t
