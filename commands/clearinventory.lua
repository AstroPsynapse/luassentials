function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.other.clearinventory")) then return end
  sender:getInventory():clear()
  sender:sendMessage("Your inventory is now clear.")
end
return t
