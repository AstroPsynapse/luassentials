function t(sender, args)
  if (not checkPerm(sender, "luassentials.admin")) then return end
  if (args[1] == nil) then
    sender:sendMessage("=====================================")
    sender:sendMessage("LUAssentials "..this.version)
    sender:sendMessage("Commands: reload, config")
    return
  end
  if (string.lower(args[1]) == "reload") then
    loadConfig()
    sender:sendMessage("LUAssentials "..this.version.. " configuration file has been reloaded.")
  elseif (string.lower(args[1]) == "config") then
    if (args[2] == nil) then
      sender:sendMessage("=====================================")
      sender:sendMessage("LUAssentials "..this.version)
      sender:sendMessage("Available configuration options:")
      sender:sendMessage("maxHomes, showMotdOnJoin, showRealmJoinLeaveMessages, buildingAllowed, announceWeatherChanges")
      return
    end
    local configEntry = string.lower(args[2])
    local configNode = "none"
    if (configEntry == "maxhomes") then
      if (args[3] == nil) then return sender:sendMessage("Please enter a valid number.") end
      if (tonumber(args[3]) == nil) then return sender:sendMessage("Please enter a valid number.") end
      if (tonumber(args[3]) < 0) then return sender:sendMessage("Please enter a valid number.") end
      local configChoice = tonumber(args[3])
      this.config.set("config.maxHomes", configChoice)
      sender:sendMessage("maxHomes has been updated to: " .. configChoice);
      return this.config.save()
    elseif (configEntry == "showmotdonjoin") then
      configNode = "showMotdOnJoin"
    elseif (configEntry == "showrealmjoinleavemessages") then
      configNode = "showRealmJoinLeaveMessages"
    elseif (configEntry == "buildingallowed") then
      configNode = "buildingAllowed"
    elseif (configEntry == "announceweatherchanges") then
      configNode = "announceWeatherChanges"
    else
      return sender:sendMessage("Invalid configuration node.")
    end
    if (configNode ~= "none") then
      if (args[3] == nil) then return sender:sendMessage("Please enter a valid option [true/false]") end
      local configChoice = string.lower(args[3])
      if (configChoice == "true") then
        this.config.set("config."..configNode, true)
        sender:sendMessage(configNode.." has been updated to: " .. args[3])
        return this.config.save()
      elseif (configChoice == "false") then
        this.config.set("config."..configNode, false)
        sender:sendMessage(configNode.." has been updated to: " .. args[3])
        return this.config.save()
      else
        return sender:sendMessage("Please enter a valid option [true/false]")
      end
    end
  end
end

return t
