function t(sender, args)
  if (not checkPerm(sender, "luassentials.user.motd")) then return end
  local file = io.open(this.path .. "/motd.txt", "r")
  if not file then
    sender:sendMessage("No motd.txt file was found.")
    return
  end
  for line in file:lines() do
    if line:sub(0,2) ~= "##" and line:len() >= 1 then
      sender:sendMessage(motdFormat(line))
    else
      motdFormat(line)
    end
  end
  io.close(file)
end
return t
