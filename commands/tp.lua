function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.teleport.tp")) then return end
  if (#args == 1) then
    local player = s:getPlayer(args[1])
    if (player == nil) then
      sender:sendMessage("We could not find the specified player.")
      return
    end
    sender:teleport(player:getLocation())
    sender:sendMessage("Teleported to "..player:getName())
  elseif (#args == 2) then
    if (not checkPerm(sender, "luassentials.teleport.tp.other")) then return end
    local playerA = s:getPlayer(args[1])
    local playerB = s:getPlayer(args[2])
    if (playerA == nil or playerB == nil) then
      sender:sendMessage("We could not find the specified player.")
      return
    end
    playerA:teleport(playerB:getLocation())
    sender:sendMessage("You teleported "..playerA:getName().." to "..playerB:getName())
    playerA:sendMessage("You were teleported to "..playerB:getName().." by "..sender:getName())
  else
    sender:sendMessage("Error - invalid syntax: /tp <playerA> [optional: playerB]")
  end
end
return t
