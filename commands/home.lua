function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.home.home")) then return end
  if (this.config.get("config.maxHomes") <= 0) then
    sender:sendMessage("Homes have been disabled.")
    return
  end
  if (not args[1]) then
    sender:performCommand("homes")
    return
  end
  local pUUID = sender:getUniqueId():toString()
  for i = 1,this.config.get("config.maxHomes") do
    local activeHome = this.config.get("players."..pUUID..".homes."..i..".name")
    if (activeHome ~= nil) then
      if (string.lower(args[1]) == string.lower(activeHome)) then
        -- Teleport to found home
        local world = s:getWorld(this.config.get("players."..pUUID..".homes."..i..".world"))
        local x, y, z = this.config.get("players."..pUUID..".homes."..i..".x"), this.config.get("players."..pUUID..".homes."..i..".y"), this.config.get("players."..pUUID..".homes."..i..".z")
        local pitch = this.config.get("players."..pUUID..".homes."..i..".pitch")
        local yaw = this.config.get("players."..pUUID..".homes."..i..".yaw")
        location = luajava.newInstance("org.bukkit.Location", world, x, y, z, yaw, pitch)
        sender:teleport(location)
        return
      end
    end
  end
  sender:sendMessage("Error - '"..args[1].."' was not found in your list of saved homes.")
end
return t
