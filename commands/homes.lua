function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.home.home")) then return end
  if (this.config.get("config.maxHomes") <= 0) then
    sender:sendMessage("Homes have been disabled.")
    return
  end
  local pUUID = sender:getUniqueId():toString()
  local homeList = ""
  for i = 1,this.config.get("config.maxHomes") do
    local activeHome = this.config.get("players."..pUUID..".homes."..i..".name")
    if (activeHome ~= nil) then
      if (homeList == "") then
        homeList = activeHome
      else
        homeList = homeList .. ", " .. activeHome
      end
    end
  end
  if (homeList == "") then
    sender:sendMessage("No saved homes were found.")
  else
    sender:sendMessage("Homes available: " .. homeList)
  end
end
return t
