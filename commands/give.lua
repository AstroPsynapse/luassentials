function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.moderation.item.other")) then return end
  if (#args >= 2) then
    local player = server:getPlayer(args[1])
    if (player == nil) then
      sender:sendMessage("Player not found."); return; end
    if (material:getMaterial(string.upper(args[2])) == nil) then
      sender:sendMessage("Invalid item name."); return; end
    local mat = material:valueOf(string.upper(args[2]))
    local amt = 1
    if (#args >= 3) then
      amt = tonumber(args[3])
      if (amt == nil) then
        sender:sendMessage("Invalid amount.")
        return
      end
    end
    itemHelpers.giveItem(player, mat, amt)
    sender:sendMessage("Given "..amt.." "..args[2].." to "..player:getName())
    player:sendMessage("You received "..amt.." "..args[2]..".")
  else
    sender:sendMessage("Invalid syntax! Usage: /give <player> <item> [optional:amount]")
  end
end
return t
