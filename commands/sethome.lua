function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.home.sethome")) then return end
  if (this.config.get("config.maxHomes") <= 0) then
    sender:sendMessage("Homes have been disabled.")
    return
  end
  if (not args[1]) then
    sender:sendMessage("Please specify a name for your home location! (eg '/sethome home1')")
    return
  end
  local pUUID = sender:getUniqueId():toString()
  local maxHomesReached = true
  local homeIndex = -1
  for i = 1,this.config.get("config.maxHomes") do
    local activeHome = this.config.get("players."..pUUID..".homes."..i..".name")
    if (activeHome ~= nil) then
      if (string.lower(args[1]) == string.lower(activeHome)) then
        -- Replace already existing
        homeIndex = i
        maxHomesReached = false
        break
      end
    else
      -- Find first empty
      homeIndex = i
      maxHomesReached = false
      break
    end
  end
  if (not maxHomesReached and homeIndex ~= -1) then
    this.config.set("players."..pUUID..".homes."..homeIndex..".name", args[1])
    this.config.set("players."..pUUID..".homes."..homeIndex..".world", sender:getLocation():getWorld():getName())
    this.config.set("players."..pUUID..".homes."..homeIndex..".x", sender:getLocation():getX())
    this.config.set("players."..pUUID..".homes."..homeIndex..".y", sender:getLocation():getY())
    this.config.set("players."..pUUID..".homes."..homeIndex..".z", sender:getLocation():getZ())
    this.config.set("players."..pUUID..".homes."..homeIndex..".pitch", sender:getLocation():getPitch())
    this.config.set("players."..pUUID..".homes."..homeIndex..".yaw", sender:getLocation():getYaw())
    this.config.save()
    sender:sendMessage("§7Your new home has been saved as '"..args[1].."'.")
  else
    sender:sendMessage("§7Error - You already have the maximum number of homes.")
  end
end
return t
