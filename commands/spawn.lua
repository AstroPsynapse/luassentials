function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.teleport.spawn")) then return end
  sender:teleport(sender:getWorld():getSpawnLocation())
  sender:sendMessage("You were teleported to the world's spawn point.")
end
return t
