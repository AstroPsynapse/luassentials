function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.home.delhome")) then return end
  if (this.config.get("config.maxHomes") <= 0) then
    sender:sendMessage("Homes have been disabled.")
    return
  end
  if (not args[1]) then
    sender:sendMessage("Please specify a home location! (eg '/delhome home1')")
    return
  end
  local pUUID = sender:getUniqueId():toString()
  for i = 1,this.config.get("config.maxHomes") do
    local activeHome = this.config.get("players."..pUUID..".homes."..i..".name")
    if (activeHome ~= nil) then
      if (string.lower(args[1]) == string.lower(activeHome)) then
        -- Delete found home
        this.config.clear("players."..pUUID..".homes."..i)
        this.config.save()
        sender:sendMessage("'"..args[1].."' has been deleted from your list of saved homes.")
        return
      end
    end
  end
  sender:sendMessage("Error - '"..args[1].."' was not found in your list of saved homes.")
end
return t
