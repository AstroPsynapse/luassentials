function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.teleport.back")) then return end
  local pUUID = sender:getUniqueId():toString()
  if (this.config.get("players."..pUUID..".lastLocation.world") == nil) then
    sender:sendMessage("There is no where to go back to.")
    return
  end
  local location = luajava.newInstance("org.bukkit.Location",
  s:getWorld(this.config.get("players."..pUUID..".lastLocation.world")),
  this.config.get("players."..pUUID..".lastLocation.x"),
  this.config.get("players."..pUUID..".lastLocation.y"),
  this.config.get("players."..pUUID..".lastLocation.z"),
  this.config.get("players."..pUUID..".lastLocation.yaw"),
  this.config.get("players."..pUUID..".lastLocation.pitch"))
  sender:teleport(location)
end
return t
