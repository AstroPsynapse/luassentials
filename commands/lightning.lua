function t(sender, args)
  if (isConsole(sender)) then return end
  if (not checkPerm(sender, "luassentials.other.lightning")) then return end
  local block = sender:getTargetBlock(null, 100);
  local location = block:getLocation()
  sender:getWorld():strikeLightning(location)
end
return t
