local luassentials = lukkit.addPlugin("luassentials", "v1.1",
	function(plugin)
		-- Global variables
		s = server
		log = s:getLogger()
		plugin.prefix = "[LUAssentials]"
		plugin.package = string.gsub(string.gsub(plugin.path, "/", "."), "\\", ".")
		this = plugin
		-- utils namespace
		loadConfig = require(this.package..'.utils.loadConfig')
		checkPerm = require(this.package..".utils.checkPerm")
		isConsole = require(this.package..".utils.isConsole")
		motdFormat = require(this.package..".utils.motdFormat")
		-- Plugin event listeners
		-- onEnable event
		this.onEnable(function()
				log:info(this.prefix.." LUAssentials "..this.version.." has been enabled.")
				loadConfig()
			end)
		-- onDisable event
		this.onDisable(function()
				log:info(this.prefix.." LUAssentials "..this.version.." has been disabled.")
			end)
		-- Game event listeners
		-- playerJoinEvent listener
		local playerJoinEvent = require(this.package..".events.playerJoinEvent")
		events.add("playerJoin",function(event) playerJoinEvent(event) end)
		-- PlayerLeaveEvent listener
		local playerLeaveEvent = require(this.package..".events.playerLeaveEvent")
		events.add("playerLeave",function(event) playerLeaveEvent(event) end)
		-- asyncPlayerChatEvent listener
		local asyncPlayerChatEvent = require(this.package..".events.asyncPlayerChatEvent")
		events.add("asyncPlayerChat",function(event) asyncPlayerChatEvent(event) end)
		-- playerTeleportEvent listener
		local playerTeleportEvent = require(this.package..".events.playerTeleportEvent")
		events.add("playerTeleport",function(event) playerTeleportEvent(event) end)
		-- playerDeathEvent listener
		local playerDeathEvent = require(this.package..".events.playerDeathEvent")
		events.add("playerDeath",function(event) playerDeathEvent(event) end)
		-- entityDamageEvent listener
		local entityDamageEvent = require(this.package..".events.entityDamageEvent")
		events.add("entityDamage",function(event) entityDamageEvent(event) end)
		-- weatherChangeEvent listener
		local weatherChangeEvent = require(this.package..".events.weatherChangeEvent")
		events.add("weatherChange",function(event) weatherChangeEvent(event) end)
		-- playerRespawnEvent listener
		local playerRespawnEvent = require(this.package..".events.playerRespawnEvent")
		events.add("playerRespawn",function(event) playerRespawnEvent(event) end)
		-- blockBreakEvent listener
		local blockBreakEvent = require(this.package..".events.blockBreakEvent")
		events.add("blockBreak",function(event) blockBreakEvent(event) end)
		-- blockPlaceEvent listener
		local blockPlaceEvent = require(this.package..".events.blockPlaceEvent")
		events.add("blockPlace",function(event) blockPlaceEvent(event) end)
		-- Command Registers
		-- LUAssentials command
		local cmd_luassentials = require(this.package..".commands.luassentials")
		this.addCommand("luassentials", "Controls various aspects of LUAssentials.", "/luassentials <command>",
			function(sender, args) cmd_luassentials(sender, args) end)
		-- tp command
		local cmd_tp = require(this.package..".commands.tp")
		this.addCommand("tp", "Teleports to the specified player. Can also be used to teleport one player to another player.", "/tp <playerA> [playerB]",
		function(sender, args) cmd_tp(sender,args) end)
		-- back command
		local cmd_back = require(this.package..".commands.back")
		this.addCommand("back", "Returns the player back to their previous position.", "/back",
			function(sender, args) cmd_back(sender,args) end)
		-- mute command
		local cmd_mute = require(this.package..".commands.mute")
		this.addCommand("mute", "Mutes or unmutes the specified player.", "/mute <name>",
			function(sender, args) cmd_mute(sender, args)	end)
		-- sethome command
		local cmd_sethome = require(this.package..".commands.sethome")
		this.addCommand("sethome", "Sets your current location as a home.", "/sethome <name>",
			function(sender, args) cmd_sethome(sender, args) end)
		-- delhome command
		local cmd_delhome = require(this.package..".commands.delhome")
		this.addCommand("delhome", "Deletes the specified home from your list of saved homes.", "/delhome <name>",
			function(sender, args) cmd_delhome(sender, args) end)
		-- home command
		local cmd_home = require(this.package..".commands.home")
		this.addCommand("home", "Teleports to the specified home location.", "/home <name>",
			function(sender, args) cmd_home(sender, args) end)
		-- item command
		local cmd_item = require(this.package..".commands.item")
		this.addCommand("item", "Grants the player the specified item.", "/item <name> [amount]",
			function (sender, args) cmd_item(sender, args) end)
		-- give command
		local cmd_give = require(this.package..".commands.give")
		this.addCommand("give", "Gives the specified player the specified item.", "/give <player> <item> [amount]",
			function (sender, args) cmd_give(sender, args) end)
		-- homes command
		local cmd_homes = require(this.package..".commands.homes")
		this.addCommand("homes", "Lists all of the player's saved homes.", "/homes",
			function(sender, args) cmd_homes(sender, args) end)
		-- motd command
		local cmd_motd = require(this.package..".commands.motd")
		this.addCommand("motd", "Shows the server's message of the day.", "/motd",
			function(sender, args) cmd_motd(sender, args) end)
		-- spawn command
		local cmd_spawn = require(this.package..".commands.spawn")
		this.addCommand("spawn", "Teleports the player to the world's spawn point.", "/spawn",
			function(sender, args) cmd_spawn(sender, args) end)
		-- setspawn command
		local cmd_setspawn = require(this.package..".commands.setspawn")
		this.addCommand("setspawn", "Sets the spawn point to the player's current position.", "/setspawn",
			function(sender, args) cmd_setspawn(sender, args) end)
		-- clearinventory command
		local cmd_clearinventory = require(this.package..".commands.clearinventory")
		this.addCommand("ci", "Completely clears a player's inventory.", "/ci",
			function(sender, args) cmd_clearinventory(sender, args) end)
		-- lightning command
		local cmd_lightning = require(this.package..".commands.lightning")
		this.addCommand("lightning", "Casts a lightning strike where the player is currently looking.", "/lightning",
			function(sender, args) cmd_lightning(sender, args) end)
	end
)
